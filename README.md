Required thirdparty libraries have been included in this project. Please run
the following commands to install

```sh
mkdir build && cd build
../configure
make
make test
make install
cd ..
```