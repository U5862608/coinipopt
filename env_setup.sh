#!/usr/bin/env bash
set -e

# download ampl demo version
# uncomment the proper download link based on your platform
filename="ampl.linux64"
# filename="ampl.linux32"
wget https://ampl.com/demo/$filename.tgz

tar xzf $filename.tgz
mv $filename ampl
cd ampl

# download and compile ipopt 
git clone https://gitlab.anu.edu.au/U5862608/coinipopt.git
cd coinipopt
mkdir build && cd build
../configure > /dev/null
echo "Start compiling.... This could take a while..."
make > /dev/null
make test > /dev/null
make install > /dev/null
cp bin/ipopt ../..
cd ../..
echo "PATH=$PWD:$PATH" >> ~/.Renviron
echo "Installation completed! The path to AMPL is: $PWD. I have added the path to your .Renviron, so you should be able to run HawkesFit directly."